# MutanApi

MutantApi es una solución que permite identificar si una cadena de ADN dada coincide con el patron
de ADN de un Mutante. Tambien permite consultar las estadicticas de analisis asociadas a las peticiones anteriores.

## Instalación y Configuración

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Herramientas necesarias

_Este proyecto usa las siguientes tecnologías para su desarrollo y funcionamineto_

* [Java (Jdk)](https://www.oracle.com/co/java/technologies/javase-jdk11-downloads.html) - El lenguaje prinicipal de desarrollo (Versión minima -> 11)
* [MySql](https://dev.mysql.com/downloads/mysql/) - Motor de Base de datos (Versión minima -> 8)
* [Spring Tool Suite](https://spring.io/tools) - IDE (Pero se puedes usar el que prefieras)
* [Cliente Sql] - Que te permita hacer consultas
* [Cliente REST] - Que te permita hacer pruebas de tipo REST (Como PostMan o Insomnia)
* [Git](https://git-scm.com/) - Para el control de Versiones



### Instalación

* 1) El primer paso es instalar las herramientas de trabajo en tu equipo (IDE, MySql server, Git etc). El servidor SQL debe ser visible desde tu proyecto para poder entablar las conexiones.

* 2) [Descarga el codigo fuente del proyecto (https://gitlab.com/arsensys/MutantApi/-/tree/main/)] - O puedes clonarlo con el siguiente comando desde una terminal:

```
git clone https://gitlab.com/arsensys/MutantApi.git
```

* 3)  Importa el poryecto (Codigo fuente)  a tu IDE. EL proyecto esta construido con Maven por loq ue el Ide debe tener soporte para ello. En caso contrario debes hacer la instalación manual y trabajar desde la consola.

* 4)  Para crear al base de datos usa el archivo **Mutant_database_struct.sql** que se encuentra en la raiz del proyecto e importalo medinte el cliente para MySql. Si usas una terminal puedes ejecutar el comando: 

```
mysql -u username -p mutant_storage < /ruta/a/tu/archivo/Mutant_database_struct.sql
```
Debes tener en cuenta que ya debe existir una base de datos con el nombre **mutant_storage**

Si tienes una configuración de servidor de base datos distinta DEbes actusalizar el archivo **application.properties** ubicado en al ruta **src/main/resources** de tu proyecto.




---

## Probar  la aplicación

* 1) Desde tu Ide levanta o inicia tu proyecto. Desde **Spring Tool Suite** puedes ejecutar el siguiente menú: 

```
Clic Derecho sobre tu proyecto (en el explorador) -->  Run As  --> Spring Boot App
```

El IDE desplegará el proyecto de manera local en el puerto 8080.

**NOTA:** La base de datos debe estar en ejecución para que puedas establecer conexiones a la misma.


* 2)  Abre el cliente de peticiones REST ( Por ejmplo PostMan o Insomnia) y crea uan nueva peticion teniendo en cuenta la **URL** y el **PUERTO** en el que se desplegó la aplicación. Un Ejemplo sería:

```
http://localhost:8080/mutant
```

En el paquete **labs.mercadolibre.mutantapi.controller** existen todos los EndPoints expuestos, por lo que puedes buscar uno para probar. Por ejemplo para probar el endpoint que verificar una cadena de ADN, usar el sigueinte EndPoint:

```
http://localhost:8080/mutant
```

El tipo de peticion debe ser:

```
POST
```

Como parametro se debe enviar un objeto con las secuencias de ADN:

```
{
"dna":["YTGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}
```
---
### Prueba de EndPoints

Puedes ver la Api desplegada aqui: http://3.16.143.5:8080/

Puedes consumir alguno de los siguientes EndPoints

```
Nobre             Tipo          Requiere Parametros  Url

Verificar ADN     POST          SI                   http://3.16.143.5:8080/mutant
Estadisticas      GET           NO                   http://3.16.143.5:8080/stats
```
---
 
