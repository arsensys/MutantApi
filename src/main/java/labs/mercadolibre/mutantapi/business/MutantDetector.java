package labs.mercadolibre.mutantapi.business;

import java.util.stream.Stream;

public class MutantDetector {

	private MutantDetectorConfig mutantDetectorConfig;

	public MutantDetector(MutantDetectorConfig mutantDetectorConfig) {
		super();
		this.mutantDetectorConfig = mutantDetectorConfig;
	}

	public MutantDetectorConfig getMutantDetectorConfig() {
		return mutantDetectorConfig;
	}

	private boolean hasConfig() {
		return (this.mutantDetectorConfig != null && this.mutantDetectorConfig.getMutantDnaPattern() != null);
	}

	/*
	 * Determina si un conjunto de secuencias de ADN coincide con la de un mutante.
	 */
	public boolean isMutant(String[] mutantDnaSequences) {

		boolean mutanDnaDetected = false;
		long matchedSequences = 0;

		if (!mutantDnaIsEmpty(mutantDnaSequences) && this.hasConfig()) {
			String[][] dnaMatrix = this.createDnaMatrixFromDnaSequences(mutantDnaSequences);
			
			if (!this.isNull(dnaMatrix)) {
				matchedSequences += getMatchesSequences(dnaMatrix);

				if (matchedSequences < this.mutantDetectorConfig.getMutantDnaSequencesCount()) {
					String[][] rotatedDnaMatrix = this.getRightRotatedDnaMatrix(dnaMatrix);
					String[][] diagonalDnaMatrix = this.getDiagonalDnaSequences(dnaMatrix);
					
					matchedSequences += getMatchesSequences(rotatedDnaMatrix);
					matchedSequences += getMatchesSequences(diagonalDnaMatrix);
				}

				mutanDnaDetected = matchedSequences >= this.mutantDetectorConfig.getMutantDnaSequencesCount();
			}
		}
		return mutanDnaDetected;
	}
	
	private long getMatchesSequences(String[][] sequences) {	
		return Stream.of(sequences).map((dnaSequence) -> {
			return String.join("", dnaSequence);
		}).filter((dna) -> {
			return this.dnaPatternMatches(dna);
		}).count();
	}
	

	/*
	 * Determina si una secuencia de ADN especifica contiene repeticiones para las proteinas
	 * establecidas en la actual configuracion de la clase.
	 */
	private boolean dnaPatternMatches(String dnaSequence) {
		boolean secuenceMatch = false;

		if (dnaSequence != null) {
			secuenceMatch = dnaSequence.matches(this.mutantDetectorConfig.getMutantDnaPattern());
		}

		return secuenceMatch;
	}
	

	private boolean mutantDnaIsEmpty(String[] mutantDna) {
		return (mutantDna == null || mutantDna.length == 0);
	}
	

	private boolean isNull(Object o) {
		return (o == null);
	}

	
	private String[][] createDnaMatrixFromDnaSequences(String[] dnaSequences) {
		String[][] dnaMatrix = null;

		if (!mutantDnaIsEmpty(dnaSequences) && !this.isNull(dnaSequences[0])) {
			int maxRowDimension =  getLargestRowDimensionFromDnaSequences(dnaSequences);
			int matrixDimension = (dnaSequences.length > maxRowDimension) ? dnaSequences.length : maxRowDimension;
			dnaMatrix = new String[matrixDimension][matrixDimension];

			for (int index = 0; index < dnaSequences.length; index++) {
				if (!isNull(dnaSequences[index])) {
					int dnaCodeIndex = 0;
					for (String dnaCode : dnaSequences[index].split("")) {
						dnaMatrix[index][dnaCodeIndex] = dnaCode;
						dnaCodeIndex++;
					}
				}
			}
		}

		return dnaMatrix;
	}
	
	private int getLargestRowDimensionFromDnaSequences(String[] dnaSequences) {
		int maxLength = 0;
		if(!this.isNull(dnaSequences)) {
			for (String dnaSequence : dnaSequences) {
				maxLength = (!this.isNull(dnaSequence) && dnaSequence.length() > maxLength) ? dnaSequence.length() : maxLength;
			}
		}
		
		return maxLength;
	}


	private String[][] getRightRotatedDnaMatrix(String[][] mutantDna) {
		String[][] rotatedMutantDna = null;
		
		if (!isNull(mutantDna)) {
			rotatedMutantDna = new String[mutantDna.length][mutantDna[0].length];
			int columIndex = mutantDna[0].length - 1;
			
			for (int i = 0; i < mutantDna.length; i++) {
				if(!isNull(mutantDna[i])) {
					for (int j = 0; j < mutantDna[i].length; j++) {
						rotatedMutantDna[j][columIndex] = mutantDna[i][j];					
					}
				}				
				columIndex--;				
			}
		}

		return rotatedMutantDna;
	}


	private String[][] getDiagonalDnaSequences(String[][] mutantDnaMatrix) {
		String[][] diagonalMutantDna = null;
		
		if (!isNull(mutantDnaMatrix)) {			
			diagonalMutantDna = new String[2][mutantDnaMatrix[0].length];
			int leftDiagonalIndex = 0;
			int rightDiagonalIndex = mutantDnaMatrix.length - 1;
			
			try {
				for (int index = 0; index < mutantDnaMatrix.length; index++) {
					diagonalMutantDna[0][index] = mutantDnaMatrix[index][leftDiagonalIndex];
					diagonalMutantDna[1][index] = mutantDnaMatrix[index][rightDiagonalIndex];
					leftDiagonalIndex++;
					rightDiagonalIndex--;
				}
			} catch (ArrayIndexOutOfBoundsException outOfbounds) {
				System.out.println("Error: malformed DNA map (Must be NxN).");
			}
		}

		return diagonalMutantDna;
	}
}
