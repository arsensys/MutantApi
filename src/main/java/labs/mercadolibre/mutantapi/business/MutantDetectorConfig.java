package labs.mercadolibre.mutantapi.business;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MutantDetectorConfig {
	private HashSet<String> dnaCodes = new HashSet<String>(List.of("A", "T", "C", "G"));;
	private int mutantDnaPatternCount;
	private int mutantDnaSequencesCount;
	private String mutantDnaPattern;

	public MutantDetectorConfig(int mutantDnaPatternCount, int mutantDnaSequencesCount, String mutantDnaPattern) {
		super();
		this.mutantDnaPatternCount = mutantDnaPatternCount;
		this.mutantDnaSequencesCount = mutantDnaSequencesCount;
		this.mutantDnaPattern = mutantDnaPattern;
	}
	
	public MutantDetectorConfig(HashSet<String> dnaCodes) {
		super();
		this.dnaCodes = dnaCodes;
	}

	public MutantDetectorConfig() {
		super();
		this.mutantDnaPatternCount = 4;
		this.mutantDnaSequencesCount = 2;
	}

	public Set<String> getDnaCodes() {
		return dnaCodes;
	}

	public void setDnaCodes(HashSet<String> dnaCodes) {
		this.dnaCodes = dnaCodes;
	}

	public void addDnaCode(String dnaCode) {
		if (dnaCode != null) {
			this.dnaCodes.add(dnaCode);
		}
	}

	public void removeDnaCode(String dnaCode) {
		if (dnaCode != null) {
			this.dnaCodes.remove(dnaCode);
		}
	}

	public int getMutantDnaPatternCount() {
		return mutantDnaPatternCount;
	}

	public void setMutantDnaPatternCount(int mutantDnaPatternCount) {
		this.mutantDnaPatternCount = mutantDnaPatternCount;
	}

	public int getMutantDnaSequencesCount() {
		return mutantDnaSequencesCount;
	}

	public void setMutantDnaSequencescount(int mutantDnaSequencesCount) {
		this.mutantDnaSequencesCount = mutantDnaSequencesCount;
	}

	public String getMutantDnaPattern() {
		return mutantDnaPattern;
	}

	public void setMutantDnaPattern(String mutantDnaPattern) {
		this.mutantDnaPattern = mutantDnaPattern;
	}

	/*
	 * Construye una expresion regular para validar el patron de adn teniendo en
	 * cuenta la condiguracion del clase actual: 'dnaCodes', 'mutantDnaPatternCount'
	 * y 'mutantDnaSequencesCount'
	 */
	public void buildDnaPatternFromDnaCodes() {
		if (this.dnaCodes != null) {
			String[] availablePatterns = this.dnaCodes.stream().map((dnaCode) -> {
				return ("" + dnaCode + "{" + this.mutantDnaPatternCount + ",}");
			}).toArray(String[]::new);

			this.mutantDnaPattern = (".*(" + String.join("|", availablePatterns) + ").*");
		}
	}
}
