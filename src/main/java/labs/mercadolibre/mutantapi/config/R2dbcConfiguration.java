package labs.mercadolibre.mutantapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

import dev.miku.r2dbc.mysql.MySqlConnectionConfiguration;
import dev.miku.r2dbc.mysql.MySqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;

@Configuration
@EnableR2dbcRepositories
public class R2dbcConfiguration extends AbstractR2dbcConfiguration {
  @Override
	public ConnectionFactory connectionFactory() {
		return MySqlConnectionFactory.from(MySqlConnectionConfiguration.builder()
				.host("mutant-api-storage.cfbhkz6d86js.us-east-2.rds.amazonaws.com")
				.username("admin")
				.port(3306)
				.password("MutantApi2021")
				.database("mutant_storage")
				.build());
	}
}