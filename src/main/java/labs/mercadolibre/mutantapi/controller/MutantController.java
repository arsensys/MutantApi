package labs.mercadolibre.mutantapi.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import labs.mercadolibre.mutantapi.business.MutantDetector;
import labs.mercadolibre.mutantapi.business.MutantDetectorConfig;
import labs.mercadolibre.mutantapi.requestdto.IsMutantRequestObject;
import labs.mercadolibre.mutantapi.service.MutantService;
import reactor.core.publisher.Mono;

@RestController
public class MutantController {
	
	@Autowired
	private MutantService mutantService;
	
	@GetMapping("/")
	public String welcome() {
		return "Welcome to MutantApi";
	}
	
	@PostMapping("/mutant")
	public Mono<ResponseEntity<String>> isMutant(@RequestBody IsMutantRequestObject mutantRequest){

		return Mono.just(new MutantDetector(new MutantDetectorConfig())).map((mutanDetector) -> {			
			mutanDetector.getMutantDetectorConfig().buildDnaPatternFromDnaCodes();			
			boolean isMutant = mutanDetector.isMutant(mutantRequest.getDna());
			mutantService.saveMutantAnalysis(mutantRequest.toString(), isMutant).subscribe();
			return (isMutant)
					? new ResponseEntity<>("La cadena de ADN contiene el patrón de un Mutante", HttpStatus.OK) 
					: new ResponseEntity<>("No es Mutante. Es Humano!", HttpStatus.FORBIDDEN) ;
		});
	}
	
}
