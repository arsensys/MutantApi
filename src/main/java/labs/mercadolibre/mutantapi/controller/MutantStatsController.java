package labs.mercadolibre.mutantapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import labs.mercadolibre.mutantapi.requestdto.MutantStatsDto;
import labs.mercadolibre.mutantapi.service.MutantService;
import reactor.core.publisher.Mono;

@RestController
public class MutantStatsController {
	
	@Autowired
	private MutantService mutantService;
	
	@GetMapping("/stats")
	public Mono<MutantStatsDto> meet(){
		return mutantService.getMutantStats();
	}
		
	@GetMapping("/humanTestsCount")
	public Mono<Long> getHumanTestsCount(){
		return mutantService.getHumanDnaSequencesCount();
	}
	
	@GetMapping("/mutantTestsCount")
	public Mono<Long> getMutantTestsCount(){
		return mutantService.getMutantDnaSequencesCount();
	}
	
	
	@GetMapping("/test")
	public Mono<String> test(){
		return mutantService.getTest();
	}

}
