package labs.mercadolibre.mutantapi.dao;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import labs.mercadolibre.mutantapi.entity.MutantAnalysisEntity;
import labs.mercadolibre.mutantapi.requestdto.StatsCountDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface IMutanDnaDao extends ReactiveCrudRepository<MutantAnalysisEntity, Long>{
	
	@Query(value = "SELECT COUNT(IF(mut_ismut <> 'S', 1, NULL)) as human_count FROM mutant_analysis")
	Mono<Long> getHumanDnaSequencesCount();
	
	@Query(value = "SELECT COUNT(IF(mut_ismut = 'S', 1, NULL)) as mutant_count FROM mutant_analysis")
	Mono<Long> getMutantDnaSequencesCount();
	
	@Query("SELECT IF(mut_ismut = 'S', 'MUTANT', 'HUMAN') as stat_description, count(DISTINCT mut_dnase) as stat_count "
			+ " FROM mutant_analysis GROUP BY IF(mut_ismut = 'S', 'MUTANT', 'HUMAN')")
	Flux<StatsCountDto> getMutantAndHumanCounts();
}
