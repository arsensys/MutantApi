package labs.mercadolibre.mutantapi.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Table("mutant_analysis")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MutantAnalysisEntity {
	
	@Id
	@Column("mut_conse")
	private long id ;
	
	@Column("mut_dnase")
	private String dnaSequence;
	
	@Column("mut_ismut")
	private String isMutant;
}
