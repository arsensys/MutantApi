package labs.mercadolibre.mutantapi.model;


public class MutantAnalysis {
	
	private long id ;
	private String dnaSequence;
	private String isMutant;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDnaSequence() {
		return dnaSequence;
	}
	public void setDnaSequence(String dnaSequence) {
		this.dnaSequence = dnaSequence;
	}
	public String getIsMutant() {
		return isMutant;
	}
	public void setIsMutant(String isMutant) {
		this.isMutant = isMutant;
	}
}
