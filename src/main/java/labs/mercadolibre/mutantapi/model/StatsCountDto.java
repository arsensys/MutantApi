package labs.mercadolibre.mutantapi.model;

import java.math.BigInteger;

public class StatsCountDto {

	String statDescription;
	BigInteger statCount;
	
	public StatsCountDto(String statDescription, BigInteger statCount) {
		super();
		this.statDescription = statDescription;
		this.statCount = statCount;
	}
	
	public String getStatDescription() {
		return statDescription;
	}
	public void setStatDescription(String statDescription) {
		this.statDescription = statDescription;
	}
	public BigInteger getStatCount() {
		return statCount;
	}
	public void setStatCount(BigInteger statCount) {
		this.statCount = statCount;
	}
}
