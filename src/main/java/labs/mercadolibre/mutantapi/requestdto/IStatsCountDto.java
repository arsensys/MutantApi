package labs.mercadolibre.mutantapi.requestdto;


public interface IStatsCountDto {
	public String getStatDescription();
	public Integer getStatCount();

}
