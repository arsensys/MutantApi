package labs.mercadolibre.mutantapi.requestdto;

import java.util.Arrays;

public class IsMutantRequestObject {
	
	private String[] dna;

	public String[] getDna() {
		return dna;
	}

	public void setDna(String[] dna) {
		this.dna = dna;
	}
	
	@Override
	public String toString() {
		return "{dna:" + Arrays.toString(dna) + "}";
	}

}
