package labs.mercadolibre.mutantapi.requestdto;

public class MutantStatsDto {
	
	private long count_mutant_dna; 
	private long count_human_dna;
	private float ratio;
	
		
	public MutantStatsDto() {
		
	}

	public MutantStatsDto(long count_mutant_dna, long count_human_dna) {
		super();
		this.count_mutant_dna = count_mutant_dna;
		this.count_human_dna = count_human_dna;
	}

	public long getCount_mutant_dna() {
		return count_mutant_dna;
	}
	
	public void setCount_mutant_dna(long count_mutant_dna) {
		this.count_mutant_dna = count_mutant_dna;
	}
	
	public long getCount_human_dna() {
		return count_human_dna;
	}
	
	public void setCount_human_dna(long count_human_dna) {
		this.count_human_dna = count_human_dna;
	}
	
	public float getRatio() {
		return this.ratio;
	}
	
	public void setRatio(float ratio) {
		this.ratio = ratio;
	}
	
	public void buildRatio() {
		 this.ratio = (count_human_dna > 0) 
				 ? ((float) count_mutant_dna / (float) count_human_dna) 
				 : (count_mutant_dna > 0) ? 1 : 0;
	}
	
	@Override
	public String toString() {
		return "{count_mutant_dna:" + count_mutant_dna + ", count_human_dna:" + count_human_dna+", ratio:" + ratio + "}";
	}
	
	
}
