package labs.mercadolibre.mutantapi.requestdto;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StatsCountDto {
	private String statDescription;
	private Long statCount;		
}
