package labs.mercadolibre.mutantapi.service;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import labs.mercadolibre.mutantapi.dao.IMutanDnaDao;
import labs.mercadolibre.mutantapi.entity.MutantAnalysisEntity;
import labs.mercadolibre.mutantapi.requestdto.MutantStatsDto;
import labs.mercadolibre.mutantapi.requestdto.StatsCountDto;
import reactor.core.publisher.Mono;

@Service
public class MutantService {
	
	@Autowired
	private IMutanDnaDao mutantDao;
	
	
	public Mono<MutantAnalysisEntity> saveMutantAnalysis(String dnaSequence, boolean isMutant) {
		MutantAnalysisEntity mutanAnalysis = new MutantAnalysisEntity();		
		mutanAnalysis.setDnaSequence(dnaSequence);
		mutanAnalysis.setIsMutant((isMutant) ? "S" : "N");
		return this.mutantDao.save(mutanAnalysis);
	}

	public Mono<MutantStatsDto> getMutantStats() {		
		return mutantDao.getMutantAndHumanCounts()
				.collect(Collectors.toMap(StatsCountDto::getStatDescription, StatsCountDto::getStatCount))
				.map((statsMap) -> {				
					MutantStatsDto stat = new MutantStatsDto();			
					if(statsMap != null && statsMap.size() > 0) {								
						stat.setCount_human_dna(statsMap.get("MUTANT").longValue());
						stat.setCount_mutant_dna(statsMap.get("HUMAN").longValue());	
						stat.buildRatio();
					}
					
					return stat;					
				}).onErrorReturn(new MutantStatsDto());	
	}
	
	
	public Mono<Long> getHumanDnaSequencesCount() {		
		return mutantDao.getHumanDnaSequencesCount();
	}
	
	
	public Mono<Long> getMutantDnaSequencesCount() {		
		return mutantDao.getMutantDnaSequencesCount();
	}
	
	
	public Mono<String> getTest() {	
		return Mono.just("Test ok!");
	}
}
