package labs.mercadolibre.mutantapi.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MutantDetectorConfigTest {

	
	@Test
	@DisplayName("Verificar la correcta configuración de MutantDetectorConfig")
	public void testDnaConfig() {
		
		MutantDetectorConfig mutanDetectoeConfig = new MutantDetectorConfig(4, 2, null);			
		assertNull(mutanDetectoeConfig.getMutantDnaPattern());
		assertTrue(mutanDetectoeConfig.getMutantDnaPatternCount() == 4);
		assertTrue(mutanDetectoeConfig.getMutantDnaSequencesCount() == 2);
		
		mutanDetectoeConfig.buildDnaPatternFromDnaCodes();
		assertEquals(".*(A{4,}|C{4,}|T{4,}|G{4,}).*", mutanDetectoeConfig.getMutantDnaPattern());
		
	}
	
	
	@Test
	@DisplayName("Verificar la correcta configuracion de las letras de ADN")
	public void tesDnaConfigCodes() {
		
		MutantDetectorConfig mutanDetectoeConfig = new MutantDetectorConfig();
		HashSet<String> dnaCodes =  new HashSet<String>();
		dnaCodes.add("X");
		dnaCodes.add("Y");
		dnaCodes.add("Z");
		mutanDetectoeConfig.setDnaCodes(dnaCodes);
		
		MutantDetectorConfig mutanDetectoeConfig2 = new MutantDetectorConfig(dnaCodes);

		assertNotNull(mutanDetectoeConfig2.getDnaCodes());
		assertNotNull(mutanDetectoeConfig.getDnaCodes());
		
		mutanDetectoeConfig.addDnaCode("A");		
		assertEquals(mutanDetectoeConfig.getDnaCodes().size(), 4);
		
		mutanDetectoeConfig.removeDnaCode("A");		
		assertEquals(mutanDetectoeConfig.getDnaCodes().size(), 3);

		
	}
}
