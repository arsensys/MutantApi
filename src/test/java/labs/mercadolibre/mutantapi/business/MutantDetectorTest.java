package labs.mercadolibre.mutantapi.business;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MutantDetectorTest {
	
	private final String[] dna = { "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG" };
	
	
	@Test
	@DisplayName("Verificar si una cadena de ADN es de MUTANTE")
	public void testHumanDna() {
		
		MutantDetector mutantDetector = new MutantDetector(new MutantDetectorConfig());
		mutantDetector.getMutantDetectorConfig().setMutantDnaSequencescount(3);
		mutantDetector.getMutantDetectorConfig().setMutantDnaPatternCount(5);
		mutantDetector.getMutantDetectorConfig().buildDnaPatternFromDnaCodes();
		
		assertFalse(mutantDetector.isMutant(dna));	
	}
	
	
	@Test
	@DisplayName("Verificar si una cadena de ADN es de HUMANO")
	public void testMutanDna() {		
		MutantDetector mutantDetector = new MutantDetector(new MutantDetectorConfig(4, 2, null));
		mutantDetector.getMutantDetectorConfig().buildDnaPatternFromDnaCodes();		
		assertTrue(mutantDetector.isMutant(dna));	
	}
	
	
	@Test
	@DisplayName("Verificar resultado de ADN NULL")
	public void testNullDna() {		
		MutantDetector mutantDetector = new MutantDetector(new MutantDetectorConfig(4, 2, null));
		mutantDetector.getMutantDetectorConfig().buildDnaPatternFromDnaCodes();		
		assertFalse(mutantDetector.isMutant(null));	
	}
	
	@Test
	@DisplayName("Verificar resultado de ADN vacia")
	public void testEmptyDna() {		
		MutantDetector mutantDetector = new MutantDetector(new MutantDetectorConfig(4, 2, null));
		mutantDetector.getMutantDetectorConfig().buildDnaPatternFromDnaCodes();		
		assertFalse(mutantDetector.isMutant(new String[4]));	
	}

}
