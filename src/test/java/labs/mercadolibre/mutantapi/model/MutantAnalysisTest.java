package labs.mercadolibre.mutantapi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


public class MutantAnalysisTest {
	
	
	
	@Test
	@DisplayName("Verificar correcto funcionamiento de MutantAnalysis")
	public void testMutantAnalysis() {
		MutantAnalysis mutantAnalysis = new MutantAnalysis();
		mutantAnalysis.setDnaSequence("['ACGT']");
		mutantAnalysis.setId(10000);
		mutantAnalysis.setIsMutant("S");
		
		assertNotNull(mutantAnalysis.getDnaSequence());
		assertEquals("['ACGT']", mutantAnalysis.getDnaSequence());
		assertTrue(mutantAnalysis.getId() == 10000);
		assertEquals("S", mutantAnalysis.getIsMutant());		
	}

}
