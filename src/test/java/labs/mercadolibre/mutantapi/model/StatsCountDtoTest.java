package labs.mercadolibre.mutantapi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigInteger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StatsCountDtoTest {

	
	@Test
	@DisplayName("Verificar correcto funcionamiento de StatsCountDto")
	public void testMutantAnalysis() {
		StatsCountDto statsCountDto = new StatsCountDto("Test",  BigInteger.valueOf(35000));
						
		assertNotNull(statsCountDto.getStatDescription());
		assertEquals("Test", statsCountDto.getStatDescription());
		
		statsCountDto.setStatDescription("New Test");
		statsCountDto.setStatCount(BigInteger.valueOf(100000));
		
		assertEquals("New Test", statsCountDto.getStatDescription());
		assertEquals(BigInteger.valueOf(100000), statsCountDto.getStatCount());	
	}
}
