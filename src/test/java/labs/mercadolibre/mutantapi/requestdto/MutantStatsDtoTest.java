package labs.mercadolibre.mutantapi.requestdto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MutantStatsDtoTest {
	
	
	@Test
	@DisplayName("Verificar correcto funcionamiento de MutantStatsDto")
	public void testMutantAnalysis() {
		MutantStatsDto statsCountDto = new MutantStatsDto(100, 100);
		MutantStatsDto statsCountDto2 = new MutantStatsDto();
		
		assertEquals(statsCountDto.getCount_human_dna(), statsCountDto.getCount_mutant_dna());
		statsCountDto.buildRatio();
		assertTrue(statsCountDto.getCount_human_dna() == 100);
		assertTrue(statsCountDto.getRatio() == 1);
						
		statsCountDto2.setCount_human_dna(5000);
		statsCountDto2.setCount_mutant_dna(10500);
		statsCountDto2.buildRatio();

		assertTrue((statsCountDto2.getCount_mutant_dna() - statsCountDto2.getCount_human_dna()) == 5500);
		assertNotNull(statsCountDto.toString());
		
		statsCountDto.setRatio(5000);
		assertTrue(statsCountDto.getRatio() == 5000);
		
	}
	
	
	@Test
	@DisplayName("Verificar correcto funcionamiento de IsMutantRequestObject")
	public void testIsMutantRequestObject() {
		IsMutantRequestObject ismutantRequest = new IsMutantRequestObject();	
		ismutantRequest.setDna(new String[]{"ATGC", "TGGA", "TCCC"});
		
		assertEquals(3, ismutantRequest.getDna().length);
		assertEquals("{dna:" + Arrays.toString(ismutantRequest.getDna()) + "}", ismutantRequest.toString());		
	}
	
	
	@Test
	@DisplayName("Verificar correcto funcionamiento de StatsCountDto")
	public void testStatsCountDto() {
		StatsCountDto statsCountDto = new StatsCountDto();
		StatsCountDto statsCountDto2 = new StatsCountDto("Test description 2", Long.valueOf(1000));
		
		statsCountDto.setStatDescription("Test description");
		statsCountDto.setStatCount(Long.valueOf(5000));
		
		assertNotNull(statsCountDto.toString());
		assertNotNull(statsCountDto2.toString());
		
		assertEquals("Test description 2", statsCountDto2.getStatDescription());
		assertEquals(1000, statsCountDto2.getStatCount());		
	}

}
