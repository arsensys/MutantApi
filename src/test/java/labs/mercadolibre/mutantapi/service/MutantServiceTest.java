package labs.mercadolibre.mutantapi.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import labs.mercadolibre.mutantapi.requestdto.MutantStatsDto;
import reactor.core.publisher.Mono;

@SpringBootTest
public class MutantServiceTest {
	
	
	@Autowired
	private MutantService mutantService;
	
	@Test
	@DisplayName("Verificar que se esten obteniendo estadisticas desde BD")
	public void testStatsResult() {		
		Mono<MutantStatsDto> mutantStats =  mutantService.getMutantStats();		
		assertNotNull(mutantStats);	
	}
	
	@Test
	@DisplayName("Verificar contero de registro para mutantes")
	public void testMutantCount() {		
	 mutantService.getMutantDnaSequencesCount().subscribe(
		 (count) -> {
			 assertTrue(count >= Long.valueOf(0));
		 }
	 );
							
	}
		
	@Test
	@DisplayName("Verificar conteo de registro para mutantes")
	public void testHumanCount() {		
	 mutantService.getHumanDnaSequencesCount().subscribe(
		 (count) -> {
			 assertTrue(count >= Long.valueOf(0));
		 }
	 );						
	}

}
